function alertUser(){
    return `
    <div class="alert alert-warning" role="alert">
    This is a warning alert-check it out!
    </div>`;
  }

const selectTag = document.getElementById("location");

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/locations/";
    try {
        const response = await fetch(url);
        if (!response.ok) {
            const html = alertUser();
            const alert = document.querySelector('.row');
            alert.innerHTML = html;
        } else {
            const data = await response.json();
            for (let location of data.locations) {
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const newConferenceUrl = "http://localhost:8000/api/conferences/";
                const fetchOptions = {
                    method: 'post',
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(newConferenceUrl, fetchOptions);
                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                }
            })
        }
    } catch (e) {
        console.log(e);
    }
});
