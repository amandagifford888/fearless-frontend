function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="col">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h3 class="card-title">${name}</h3>
          <h5 class="card-subtitle text-muted">${location}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            ${startDate} - ${endDate}
        </div>
      </div>
      </div>`;
  }

  function alertUser(){
    return `
    <div class="alert alert-warning" role="alert">
    This is a warning alert-check it out!
    </div>`;
  }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
    if (!response.ok) {
        const html = alertUser();
        const alert = document.querySelector('.row');
        alert.innerHTML = html;
    } else {
        const data = await response.json();
        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts).toLocaleDateString();
                const endDate = new Date(details.conference.ends).toLocaleDateString();
                const location = details.conference.location.name;
                // console.log(pictureUrl, description, title)
                const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                const row = document.querySelector('.row');
                row.innerHTML += html;
                // console.log(html);
                // console.log(details.conference.pk)
                // if (details.conference.pk % 3 === 2) {
                //     const column1 = document.querySelector('.col-1');
                //     column1.innerHTML = html;
                // } else if (details.conference.pk % 3 === 1) {
                //     const column2 = document.querySelector('.col-2');
                //     column2.innerHTML = html;
                // } else {
                //     const column3 = document.querySelector('.col-3');
                //     column3.innerHTML = html;
                // }


            }
        }


        }
    } catch (e) {
        console.log(e)
    }
});
