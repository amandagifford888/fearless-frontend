import './App.css';
import Nav from './Nav'
import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Link, Outlet } from "react-router-dom";
import LocationForm from './LocationForm.js';
import ConferenceForm from './ConferenceForm.js'
import AttendeesList from './AttendeesList';
import AttendeeForm from './AttendeeForm.js';
import PresentationForm from './PresentationForm.js';
import MainPage from './MainPage.js';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <React.StrictMode>
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route index element={<MainPage />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} /> } />
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendeeForm />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
      {/* <LocationForm /> */}
      {/* <AttendeesList attendees={props.attendees} /> */}
      {/* <ConferenceForm /> */}
    </div>
    </BrowserRouter>
    </React.StrictMode>
  );
}

export default App;
