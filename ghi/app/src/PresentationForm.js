import React, {useEffect, useState} from 'react';

function PresentationForm () {
    const [conferences, setConferences] = useState([]);
    const [presenterName, setPresenterName] = useState([]);
    const [presenterEmail, setPresenterEmail] = useState([]);
    const [companyName, setCompanyName] = useState([]);
    const [title, setTitle] = useState([]);
    const [synopsis, setSynopsis] = useState([]);
    const [conference, setConference] = useState([]);


    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect ( () => {
        fetchData();
    }, []);

    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }

    const handlePresenterEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePresenterNameChange} placeholder="Presenter name" name="presenter_name" required type="text" id="presenter_name" className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresenterEmailChange} placeholder="Presenter email" name="presenter_email" required type="email" id="presenter_email" className="form-control"/>
                <label htmlFor="starts">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company name" name="company_name" required type="text" id="company_name" className="form-control"/>
                <label htmlFor="ends">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} placeholder="Title" name="title" required type="text" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} className="form-control" name="synopsis" id="synopsis"></textarea>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleConferenceChange} name="conference" required id="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.id}>{conference.name}</option>
                        )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

}

export default PresentationForm;
